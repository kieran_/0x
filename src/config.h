#ifndef CONFIG_H
#define CONFIG_H

#include "../json11/json11.hpp"
#include "../libwebsockets/lib/libwebsockets.h"

#include <fstream>
#include <sstream>

using std::string;

namespace Ox {
	class Config{
	public:
		Config() : filename("server.cfg"), nodeName("New node"), nodeMaster(""), iface("eth0"), port(9000), sslCert(""), sslKey("") {};
		void load();
		void save();

		string filename;
		string nodeName;
		string nodeMaster;
		string iface;
		string sslCert;
		string sslKey;
		int port; 
	};
}
#endif
