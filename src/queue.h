#ifndef QUEUE_H
#define QUEUE_H

#include <mutex>

namespace Ox{
	template<typename Z>
	struct node
	{
		node(const Z& data) : data(data), next(nullptr) {}
		Z data;
		node* next;
	};


	template<typename T>
	class Queue
	{
	public:
		Queue() : head_(nullptr), nodes(0) {}
		void add(const T &data){
			lock_.lock();
			node<T>* new_node = new node<T>(data);
			if (nodes == 0){
				head_ = new_node;
			}
			else if (nodes == 1){
				tail_ = new_node;
				head_->next = new_node;
			}
			else{
				tail_->next = new_node;
				tail_ = new_node;
			}
			nodes++;
			lock_.unlock();
		}
		node<T>* consume(){
			lock_.lock();
			node<T>* h = head_;
			if (head_){
				head_ = head_->next;
				nodes--;
			}
			lock_.unlock();
			return h;
		}
		int length(){
			int n = 0;
			lock_.lock();
			n = nodes;
			lock_.unlock();
			return n;
		}
	private:
		node<T>* head_;
		node<T>* tail_;
		int nodes;
		std::mutex lock_;
	};
}
#endif