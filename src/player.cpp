#include "player.h"

using Ox::Player;

bool Player::moveTo(double x, double y, double z){
	this->x = x;
	this->y = y;
	this->z = z;
}

void Player::setVerse(Universe* v){
	this->verse = v;
}
