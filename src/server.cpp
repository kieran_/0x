#include "server.h"

using namespace v8;

Queue<Packet>* Server::q = new Queue<Packet>();
vector<Universe>* Server::verse = new vector<Universe>();

int Server::callback(struct libwebsocket_context *ctx, struct libwebsocket *wsi, enum libwebsocket_callback_reasons reason, void *user, void *in, size_t len){
	session_data* session = (session_data*)user;
	
	switch (reason) {
		case LWS_CALLBACK_ESTABLISHED:{
						
			Player* pNew = new Player(wsi,"NewPlayer");
			session->wsi = wsi;
			session->p = pNew;
			
			verse->at(0).playerJoin(pNew);
			lwsl_notice("%s: Joined the game [%s]\n",pNew->getName().c_str(), pNew->getUUID().str().c_str());
			
			break;
		}
		case LWS_CALLBACK_RECEIVE: {
			Player* p = session->p;
			Universe* u = p->getVerse();
				
			//lwsl_notice("%s sent the message: %s\n",p->getName().c_str(), (char*)in);
			if(p){
				string jj((char*)in);
				Packet packet(jj,p,u);
				
				string reply = packet.getReplyResponse();
				string broadcast = packet.getBroadcastResponse();
						
				if(!reply.empty())
					libwebsocket_write(wsi, (unsigned char*)reply.c_str(), reply.length(), LWS_WRITE_TEXT);
					
				if(!broadcast.empty())
					u->broadcast((unsigned char*)broadcast.c_str(),broadcast.length());
			}else{
				lwsl_notice("Unknown player sent data: %s\n",(unsigned char*)in);
			}
			//libwebsocket_write(wsi, (unsigned char*)s.c_str(), s.length(), LWS_WRITE_TEXT);
			
			/*Isolate* isolate = Isolate::New();{
				Isolate::Scope iscope(isolate);
				V8::Initialize();
				Locker l(isolate);
				
				HandleScope handle_scope(isolate);
				Handle<Context> context = Context::New(isolate);
				Context::Scope context_scope(context);
				Handle<String> source = String::NewFromUtf8(isolate, "'Hello' + ', World!'");
				Handle<Script> script = Script::Compile(source);
				Handle<Value> result = script->Run();
				String::Utf8Value utf8(result);

			}
			isolate->Dispose();*/

			break;
		}
		case LWS_CALLBACK_CLOSED:{
			lwsl_notice("%s Disonnected.\n",session->p->getName().c_str());
			if(session->p->getVerse()->playerLeave(session->p)){
				delete session->p;
			}else{
				lwsl_notice("Failed to delete player from universe, memory leaking.\n");
			}
			break;
		}
		default:
			break;
	}
	return 0;
}

int Server::callback_http(struct libwebsocket_context *ctx, struct libwebsocket *wsi, enum libwebsocket_callback_reasons reason, void *user, void *in, size_t len){
	return 0;
}

bool Server::init(Config* cfg){
	memset(&info, 0, sizeof info);
	
	info.iface = cfg->iface.c_str();
	info.port = cfg->port;
	info.protocols = proto;
	info.ssl_cert_filepath = !cfg->sslCert.empty() ? cfg->sslCert.c_str() : NULL;
	info.ssl_private_key_filepath = !cfg->sslKey.empty() ? cfg->sslKey.c_str() : NULL;
	info.gid = -1;
	info.uid = -1;

	context = libwebsocket_create_context(&info);
	
	if (context == NULL) {
		lwsl_notice("Server start failed");
		return false;
	}

	isRunning = true;
	main = thread(&Server::run, this);
	consumer = thread(&Server::runConsume, this);
	
	Universe u(69,"Main universe");
	verse->push_back(u);
	
	return true;
}

bool Server::restart() {
	stop();
	init(cfg);
}
void Server::run() {
	while (isRunning) {
		libwebsocket_service(context, 50);
	}
}

void Server::runConsume(){
	node<Packet>* p;
	while (isRunning){
		p = q->consume();
		if (p != NULL){
			lwsl_notice("Consuming data\n");
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

Server::~Server(){
	
}

void Server::stop(){
	isRunning = false;
	
	if(main.joinable())
		main.join();
	if(consumer.joinable())
		consumer.join();

	libwebsocket_context_destroy(context);
	lwsl_notice("Websocket server stopped\n");
}
