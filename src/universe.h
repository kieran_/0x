#ifndef UNIVERSE_H
#define UNIVERSE_H

#include <vector>

#include "../libwebsockets/lib/libwebsockets.h"
#include "player.h"
#include "packet.h"

using std::vector;
using std::string;

namespace Ox {
	class Universe {
	public:
		Universe(unsigned int i,string n) : id(i), name(n) {}
		void playerJoin(Player* p);
		bool playerLeave(Player* p);
		unsigned int getId() { return this->id; }
		string getName() { return this->name; }
		vector<Player*>* getPlayers() { return &(this->players); }
		void broadcast(unsigned char* in, size_t len); 
		
	private:
		string name;
		unsigned int id;
		vector<Player*> players;
	};
}
#endif
