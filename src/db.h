#ifndef DB_H
#define DB_H

using std::string;

namespace Ox {
	class DB {
	public:
		DB();
		
	private:
		string connectionString;
	};
}
#endif
