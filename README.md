0x
====

0x is a project by Kieran Harkin. This project will contain the server code for the game at https://0x.tf

Installation
--------------

```sh
git clone --recursive https://bitbucket.org/kieran_/0x.git
cd 0x
sudo apt-get install libssl-dev g++ cmake subversion
sudo make build
sudo make

```

Thanks to
-----------

0x uses a few other projects to help it get the job done:

* [Google V8] - googles V8 engine runs all the user scripts
* [libwebsockets] - helps us connect to the users via websockets
* [json11] - great for reading what the clients are saying and used to talk to them also
* [sole] - UUID generator
* [three.js] - the game engine 
* [jQuery] - js library that everybody should know

[Google V8]:https://code.google.com/p/v8/
[libwebsockets]:http://libwebsockets.org/trac/libwebsockets
[json11]:https://github.com/dropbox/json11/
[three.js]:http://threejs.org/
[jQuery]:http://jquery.com/
[sole]:https://github.com/r-lyeh/sole
