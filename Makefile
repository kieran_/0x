# Makefile v0.1

CC=g++
BIN=0x
BIN_DEBUG=0x_debug
LIBS=-lwebsockets -lv8 -licuuc -licui18n
LIBS_DIR=-Llibwebsockets/build/lib -Lv8/out/native/lib.target
SRC=json11/json11.cpp sole/sole.cpp src/*.cpp
DEF=-DV8_VERSION="\"$(V8_VERSION)\"" -DOX_VERSION="\"$(OX_VERSION)\"" -DLWSL_VERSION="\"$(LWSL_VERSION)\""
FLAGS=-O2 -std=c++11 -pthread -Wl,--no-as-needed

V8_VERSION= $(shell cd v8 && git log --pretty=format:'%h (%ad)' --date=short -n 1)
OX_VERSION= $(shell git log --pretty=format:'%h (%ad)' --date=short -n 1)
LWSL_VERSION= $(shell cd libwebsockets && git log --pretty=format:'%h (%ad)' --date=short -n 1)

all:
	$(CC) $(FLAGS) -o$(BIN) $(SRC) $(LIBS_DIR) $(LIBS) $(DEF)

debug:
	$(CC) -g $(FLAGS) -o$(BIN_DEBUG) $(SRC) $(LIBS_DIR) $(LIBS) $(DEF)

build:
	mkdir -p libwebsockets/build
	cd libwebsockets/build; cmake -DLIB_SUFFIX=64 -DLWS_WITHOUT_CLIENT=ON ..; make
	cp -rf libwebsockets/build/lib/* /usr/lib
	
	cd v8; make dependencies
	cd v8; make native -j4 library=shared
	cp -rf v8/out/native/lib.target/* /usr/lib

clean:
	rm -rf 0x
	rm -rf libwebsockets/build
	rm -rf v8/out
