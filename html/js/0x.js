var Player = {
	name: "Kieran",
	health: 100,
	movespeed: 10,
	lookspeed: 0.5,
	fov: 60,
	near: 0.1,
	far: 1000
};

var OX  = {
	clock: new THREE.Clock(),
	projector: new THREE.Projector(),
	scene: new THREE.Scene(),
	camera: new THREE.PerspectiveCamera( Player.fov, window.innerWidth / window.innerHeight, Player.near, Player.far ),
	controls: undefined,
	renderer: new THREE.WebGLRenderer(),
	light: new THREE.AmbientLight( 0x202020 ),
	ready: false,
	players: [],
	
	init: function(){		
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		document.body.appendChild( this.renderer.domElement );
		document.addEventListener('pointerlockchange', this.pointerLock, false);
        document.addEventListener('mozpointerlockchange', this.pointerLock, false);
        document.addEventListener('webkitpointerlockchange', this.pointerLock, false); 
		
		this.controls = new THREE.PointerLockControls(OX.camera);
		Player.camera = this.controls.getObject();
		this.controls.otherKey = OX.onKeyPress;
		this.scene.add(Player.camera);
		
        $("canvas").click(function () {
            var canvas = $("canvas").get()[0];
            canvas.requestPointerLock = canvas.requestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
            canvas.requestPointerLock();
        });
        
		this.scene.add( this.light );
		
		var geometry = new THREE.PlaneGeometry( 200, 200 );
		var material = new THREE.MeshBasicMaterial( {color: 0xcccccc } );
		var plane = new THREE.Mesh( geometry, material );
		this.scene.add( plane );
		plane.applyMatrix( new THREE.Matrix4().makeRotationX( - Math.PI / 2 ) );

		
		setInterval( function () {
			if ( ! document.webkitHidden ) requestAnimationFrame( OX.render );
		}, 1000 / 25 );
		
		this.net.init();
	},
	home: function(){
		Player.camera.position.x = 0;
		Player.camera.position.y = 0;
		Player.camera.position.z = 0;
	},
	pointerLock: function(e) {
		var canvas = $("canvas").get()[0];
        if (document.pointerLockElement === canvas || document.mozPointerLockElement === canvas || document.webkitPointerLockElement === canvas) {
            OX.controls.enabled = true;
        } else {
            OX.controls.enabled = false;
        }
	},
	update: function(delta){
		OX.controls.update();
		
		if(OX.controls.playerMoved){
			OX.net.sendMsg({
				code: 7,
				data: { 
					x: Player.camera.position.x,
					y: Player.camera.position.y,
					z: Player.camera.position.z,
					rx: Player.camera.rotation.x,
					ry: Player.camera.rotation.y,
					rz: Player.camera.rotation.z,
				}
			});
			OX.controls.playerMoved = false;
		}
		
		$("#dt").html("DT: " + delta);
		$("#px").html("X: " + Player.camera.position.x);
		$("#py").html("Y: " + Player.camera.position.y);
		$("#pz").html("Z: " + Player.camera.position.z);
	},
	
	onKeyPress: function(e){
		switch(e.keyCode){
			case 84:{
				OX.controls.enabled = !OX.controls.enabled;
				$("#chatWindow").fadeToggle(200);
				break;
			}
			case 192: {
				//show console
				
				break;
			}
			default:
				console.log("Unknown key: " + e.keyCode);
		}
		return true;
	},
	
	render: function(){
		if(OX.ready){
			var delta = OX.clock.getDelta();
			OX.update(delta);
			OX.renderer.render(OX.scene, OX.camera);
		}
	},
	
	login: function(name){
			OX.net.sendMsg({
				code: 4,
				data: {
					username: name 
				}
			});
			OX.ready = true;
			$('#loginWindow').hide();
	},
	
	net: {
		wsUri: "ws://0x.tf:9000",
		websocket: undefined,
		init: function(){
			this.websocket = new WebSocket(this.wsUri,"0x"); 
			this.websocket.onopen = this.onOpen; 
			this.websocket.onclose = this.onClose; 
			this.websocket.onmessage = this.onMessage;
			this.websocket.onerror = this.onError; 
		},
		onOpen: function(e){
			console.log("Connected");
		},
		onClose: function(e){
			console.log("Connection closed");
		},
		onMessage: function(e){
			var p = JSON.parse(e.data);
			switch(p.code){
				case 4:{
					//new player
					var geometry = new THREE.BoxGeometry( 1, 1, 1 );
					var material = new THREE.MeshBasicMaterial( {color: 0xFFF } );
					var player = new THREE.Mesh( geometry, material );
					OX.scene.add( player );
					
					player.uuid = p.uuid;
					player.name = p.name;
					
					console.log("player " + player.name + " joined the game");
					OX.players.push(player); 
					break;
				}
				case 7:{
					//player move
					var ok = false;
					for(var x = 0; x < OX.players.length; x++){
						var pl = OX.players[x];
						if(pl.uuid == p.player){
							pl.position.x = p.data.x;
							pl.position.y = p.data.y;
							pl.position.z = p.data.z;
							ok = true;
							break;
						}
					} 
					
					if(!ok)
						console.log("Player not found with uuid: " + p.player);
					break;
				}
				default:{
					//unknown mesage
					console.log("Unknown message: " + p);
				}
			}
		},
		onError: function(e){
			console.log("Connection error");
		},
		sendMsg: function(data){
			if(this.websocket.readyState == 1){
				this.websocket.send(JSON.stringify(data));
			}else{
				console.log("Socket is not ready");
			}
		}
	}
};
